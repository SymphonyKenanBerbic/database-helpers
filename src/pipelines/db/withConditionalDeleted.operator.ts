import { Repository, SelectQueryBuilder } from 'typeorm';
import { Operator } from "./operator";

export class WithConditionalDeletedOperator extends Operator {
  private readonly requestParam;

  constructor(requestParam: string) {
    super();

    this.requestParam = requestParam;
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    return q.withDeleted();
  }

  protected getRequestParamName(): string {
    return this.requestParam;
  }
}
