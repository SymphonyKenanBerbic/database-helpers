import { SearchByPropertiesOperator } from '.';

export interface SearchByPropertiesOptions{
  requestParam?: string;
  matcher?: string;
  collation?: string;
}

export class SearchByPropertiesWithOptionsOperator extends SearchByPropertiesOperator {
  constructor(
      properties: any[] | string,
      {requestParam = 'search',
      matcher = 'ILIKE',
      collation}: SearchByPropertiesOptions
  ) {
    super(properties, requestParam, matcher, collation);
  }
}

