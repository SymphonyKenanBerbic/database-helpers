import { Operator } from './operator';
import { Repository, SelectQueryBuilder } from 'typeorm';

export class HardWhereOperator extends Operator {
  private readonly property;
  private readonly value;

  constructor(property: string, value: any) {
    super();

    this.property = property;
    this.value = value;
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    const randProp = this.randomString(5);

    const paramsObject = {};
    paramsObject[randProp] = this.value;

    return q.andWhere(`${this.property} = :${randProp}`, paramsObject);
  }

  shouldHandle() {
    return true;
  }

 private randomString(size: number) {
    let result = '';
    const characters =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < size; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

}
