import { Operator } from './operator';
import { Brackets, Repository, SelectQueryBuilder } from 'typeorm';
import { isString } from 'class-validator';

export class SearchAllPropertiesOperator extends Operator {
  private properties;
  private readonly requestParamName;

  constructor(properties: string[] | string, requestParam = 'search') {
    super();

    if (isString(properties)) {
      const temp = properties.split(',');
      properties = temp.map(x => x.trim());
    }

    this.properties = properties.reverse();
    this.requestParamName = requestParam;
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    const searchTerms = this.getRequest().query[this.getRequestParamName()];

    return q.andWhere(
      new Brackets(query => {
        for (const prop of this.properties) {
          if (prop === this.properties[0]) {
            // The first element (or last technically) is a standard where clause
            query.where(`${prop}::varchar LIKE :prop`, {
              prop: `%${searchTerms}%`,
            });
          } else {
            // All other clauses are separated from the other by "OR" operators
            // Complexity: O(n) where n = count($properties)\
            query.orWhere(`${prop}::varchar LIKE :prop`, {
              prop: `%${searchTerms}%`,
            });
          }
        }
      }),
    );
  }

  protected getRequestParamName(): string {
    return this.requestParamName;
  }
}