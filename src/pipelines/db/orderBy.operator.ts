import { Operator } from './operator';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { isString } from 'class-validator';

export class OrderByOperator extends Operator {
  /**
   * Array of pairs containing the order by clauses of the resulting SQL operation.
   *
   * @var array
   */
  private defaultOrderParameters: string[][] = [];

  /**
   * If this operator should not be affected by the query params and
   * its only a defined order, this should be set to true.
   *
   * @var bool
   */
  private acceptsRequestInput: boolean;

  constructor(
    defaultOrderParameters: string[] | string | null = null,
    acceptsRequestInput = true,
  ) {
    super();

    this.defaultOrderParameters = this.buildParams(defaultOrderParameters);
    this.acceptsRequestInput = acceptsRequestInput;
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    let q = this.getQuery(passable);
    let params;

    if (
      (this.hasDefaultOrderQuery() && !this.acceptsRequestInput) ||
      (this.hasDefaultOrderQuery() &&
        !this.requestHas(this.getRequestParamName()))
    ) {
      params = this.defaultOrderParameters;
    } else if (
      this.acceptsRequestInput &&
      this.requestHas(this.getRequestParamName())
    ) {
      params = this.buildParams(
        this.getQueryParams()[this.getRequestParamName()] as string,
      );
    }

    let i = 0;
    for (const param of params) {
      if (i == 0) {
        q = q.orderBy(param[0], param[1] as any);
      } else {
        q = q.addOrderBy(param[0], param[1] as any);
      }
      i++;
    }

    return q;
  }

  /**
   * Add the parameters that will be executed by default with this operator.
   * Check @defaultNoRequestInput() to make this operator
   * not affected by the request if needed.
   *
   * @params array|string|null
   * @return OrderByOperator
   */
  public default(params: string[] | string | null) {
    this.defaultOrderParameters = this.buildParams(params);
  }

  public buildParams(params: string[] | string | null) {
    if (params === null) {
      return null;
    }

    if (isString(params)) {
      params = params.split(',');
    }

    const temp: string[][] = [];
    for (const param of params)
      if (param[0] === '-') {
        temp.push([param.substr(1, param.length).trim(), 'DESC']);
      } else if (param[0] === '+') {
        temp.push([param.substr(1, param.length).trim(), 'ASC']);
      } else {
        temp.push([param.trim(), 'ASC']);
      }

    return temp;
  }

  /**
   * This method is always called to ensure this operator should be
   * executed. By default checks if the is a query param by the
   * name given from @getRequestParamName() in the request.
   *
   * @return boolean
   */
  shouldHandle(): boolean {
    return (
      (this.requestHas(this.getRequestParamName()) &&
        this.acceptsRequestInput) ||
      this.hasDefaultOrderQuery()
    );
  }

  /**
   * Does this operator have a default order query.
   *
   * @return bool
   */
  private hasDefaultOrderQuery() {
    return (
      this.defaultOrderParameters !== null &&
      this.defaultOrderParameters !== undefined
    );
  }
}
