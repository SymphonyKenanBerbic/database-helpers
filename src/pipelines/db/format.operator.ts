import { Operator } from './operator';
import { Pagination } from '../../paginator';

export class FormatOperator extends Operator {
  constructor(private formatter: any, private funcFormatter: any = null) {
    super();
  }

  async applyOperator(data: any): Promise<any[]> {
    const d = await data;
    const toDesconstruct = d instanceof Array ? null : d;
    return {
      ...toDesconstruct as any,
      items: this.formatter.formatCollection(
          d instanceof Pagination ? d.items : d,
          this.funcFormatter,
      )
    };
  }

  /**
   * This method is always called to ensure this operator should be
   * executed. By default checks if the is a query param by the
   * name given from @getRequestParamName() in the request.
   *
   * @return boolean
   */
  shouldHandle(): boolean {
    return true;
  }
}
