import { Operator } from './operator';
import { Repository, SelectQueryBuilder } from 'typeorm';

export class WhereIdsInOperator extends Operator {
  private readonly property;
  private readonly requestParamName;

  constructor(property = 'id', requestParamName = 'ids') {
    super();

    this.property = property;
    this.requestParamName =
      requestParamName !== null ? requestParamName : property;
  }

  applyOperator(passable: Repository<any> | SelectQueryBuilder<any>): any {
    const q = this.getQuery(passable);
    const searchTerms = this.getRequest().query[
      this.getRequestParamName()
    ].toString();
    const ids = searchTerms.split(',').map(x => Number.parseInt(x));

    if (ids.find(x => isNaN(x)) !== undefined) {
      // If we are sending non numerics just abort.
      return q;
    }

    const paramsObject = {};
    paramsObject[this.getRequestParamName()] = ids;

    return q.andWhere(`${this.property} IN (:...${this.getRequestParamName()})`, paramsObject);
  }

  protected getRequestParamName(): string {
    return this.requestParamName;
  }
}
