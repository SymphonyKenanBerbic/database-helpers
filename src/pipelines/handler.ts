import { Pipeline } from './pipeline';

export abstract class Handler {
  private pipeline: Pipeline;
  protected metadata: any;

  abstract handle(passable: any): any;

  withPipeline(pipeline: Pipeline) {
    this.pipeline = pipeline;
    return this;
  }

  withMetadata(metadata: any) {
    this.metadata = metadata;
    return this;
  }

  getPipeline() {
    return this.pipeline;
  }

  getMetadata() {
    return this.metadata;
  }
}
