import {FindManyOptions, FindOneOptions, FindOptionsWhere, In, Repository} from 'typeorm';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { UpdateResultModel } from '../models/updateResult.model';
import { Request } from 'express';

export abstract class AbstractRepository<T> {
  protected constructor(private entity: Repository<T>,
                        private request?: Request) {}

  getObjectForSearch(): Repository<T> {
    return this.entity;
  }

  find(): Promise<T[]> {
    return this.entity.find();
  }

  findById(id: number): Promise<T> {
    return this.getFindOne(this.getFindOptions({ id } as any));
  }

  findByIds(ids: number[]): Promise<T[]> {
    return this.entity.find(this.getFindOptions({ id: In(ids) } as any));
  }

  save<C>(entity: C): Promise<T> {
    return this.entity.save(entity as any);
  }

  update<C>(id: number, entity: Partial<C>): Promise<UpdateResultModel<T>> {
    return this.entity
      .createQueryBuilder()
      .update()
      .set({ ...entity } as any)
      .where('id = :id', {
        id,
      })
      .returning('*')
      .execute()
      .then(async response => {
        return {
          result: response.affected > 0,
          entity: await this.getFindOne(this.getFindOptions({ id } as any)),
        };
      });
  }

  async delete(id: number): Promise<DeleteResult> {
    if (!(await this.entity.findOneByOrFail({ id } as any))) {
      return null;
    }
    return this.entity.delete(id);
  }

  async softDelete(id: number): Promise<UpdateResult> {
    if (!(await this.entity.findOneByOrFail({ id } as any))) {
      return null;
    }
    return this.entity.softDelete(id);
  }

  protected getFindOne(
      options: FindOneOptions<T>
  ): Promise<T | null>{
    return this.hasWithoutFailure() ? this.entity.findOne(options) : this.entity.findOneOrFail(options);
  }

  protected getFindOptions(
      where: FindOptionsWhere<T> | FindOptionsWhere<T>[],
  ): FindOneOptions<T> | FindManyOptions<T> {
    return {
      where,
      withDeleted: this.hasWithDeleted()
    };
  }

  protected hasWithDeleted() {
    return !!(this.request && this.request.query['with_deleted']);
  }

  protected hasWithoutFailure() {
    return !!(this.request && this.request.query['without_failure']);
  }
}
